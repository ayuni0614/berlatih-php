<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Tentukan Nilai</title>
    </head>
    <body>
        <h1>Tentukan Nilai</h1>
        <?php
        function tentukan_nilai($number)
        {
            //  kode disini
            if ($number>=85 && $number<=100) {
                return "Sangat Baik<br>";
            } else if ($number>=70 && $number<85) {
                return "Baik<br>";
            } else if ($number>=60 && $number<70) {
                return "Cukup<br>";
            } else {
                return "Kurang";
            }
        }
        echo "98 : ".tentukan_nilai(98);//Sangat Baik
		echo "76 : ".tentukan_nilai(76); //Baik
		echo "67 : ".tentukan_nilai(67); //Cukup
		echo "43 : ".tentukan_nilai(43); //Kurang
        ?>
    </body>
</html>